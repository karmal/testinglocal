


$('#table_id').DataTable( {
    ajax: {
        url: '/api/meetings',
        dataSrc: '',
        table: '',

        complete: function(response){
            // console.log(response);
            this.table = response;
            // console.log(this.table);

            localStorage.setItem("items", JSON.stringify(this.table));
        }
    },
    columns: [ 
        { data: 'name' },
        { data: 'description' },
        { data: 'id' },
        { data: 'created_at' }
    ]
} );

$("#btnClear").click(function () {  
    localStorage.clear();  
 })  
// localStorage.setItem("lastname", data.name);



