let loadData = true;
let config =  {
    columns: [ 
        { data: 'name' },
        { data: 'description' },
        { data: 'id' },
        { data: 'created_at' }
    ]
} 

// data from local storage
var myData = JSON.parse(localStorage.getItem('items'));
// if any data in local storage
if(myData){
    var dataInJson = myData.data;
    console.log('there is data in local');

    $.ajax({
        // go to api to check the data from local storage with data in api
        url: '/api/tablecheck?date='+ myData.date,
    

      // response xhr.responseJSON will be either false or true depending if it was more than 5 minutes 
        complete: (XHR)=>{
            console.log(XHR);
            loadData = (XHR.responseJSON);
            
                // if response is true(more than 5 minutes) get data from api
                if(loadData){
                    console.log('loading from api');
                    config.ajax =  {
                        url: '/api/meetings',
                        dataSrc: '',
                        table: '',
                    
                        complete: function(response){
                            // console.log(response);
                            this.table = response.responseJSON;
                            // console.log(this.table);
                    
                            localStorage.setItem("items", JSON.stringify({data: this.table, date: new Date().toISOString()}));
                
                        }
                    }
                    // else get data from local
                } else {
                    console.log('loading from local');
                    config.data = dataInJson;
                }
                $('#table_id').DataTable(config);
                


            // XHR.responseJSON ;
            // console.log(XHR.responseJSON);
        }
      });
      console.log('loading data');
} else {
            
                // if response is true(more than 5 minutes) get data from api
                if(loadData){
                    console.log('loading from api');
                    config.ajax =  {
                        url: '/api/meetings',
                        dataSrc: '',
                        table: '',
                    
                        complete: function(response){
                            // console.log(response);
                            this.table = response.responseJSON;
                            // console.log(this.table);
                    
                            localStorage.setItem("items", JSON.stringify({data: this.table, date: new Date().toISOString()}));
                
                        }
                    }
                    // else get data from local
                } else {
                    console.log('loading from local');
                    config.data = dataInJson;
                }
                $('#table_id').DataTable(config);
    
}



    

 


  $("#btnClear").click(function () {  
    localStorage.clear();  
 }) 




// localStorage.setItem("lastname", data.name);






