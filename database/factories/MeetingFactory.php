<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Meeting;
use Faker\Generator as Faker;

$factory->define(Meeting::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(6),
        'description' =>  $faker->sentence(10)
    ];
});



