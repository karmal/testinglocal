<?php

namespace App\Http\Controllers;

use App\Meeting;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
        //  return response(Meeting::all()->jsonSerialize(), Response::HTTP_OK);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
   
        // // return view('welcome', ['votes' => Vote::all(), 'meetings' => Meeting::all()]);
        // // return response()->json([
        // //     'meetings' => Meeting::latest()->get()
        // // ]);
        // $meeting = new Meeting();
        // $meeting->name = $request->name;
        // $meeting->description = $request->description;
        // $meeting->save();
        // return response($meeting->jsonSerialize(), Response::HTTP_CREATED);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),
        [
            'name' => 'required',
            'description' => 'required'
        ]);

        Meeting::Create(
        [
            'name' => request('name'),
            'description' => request('description')
        ]);

        return ['message' => 'Meeting Created !'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function show(Meeting $meeting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function edit(Meeting $meeting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $meeting = Meeting::findOrFail($id);
  $meeting->color = $request->color;
  $meeting->save();

  return response(null, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Meeting::destroy($id);

  return response(null, Response::HTTP_OK);
    }
}
