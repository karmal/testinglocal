<?php

namespace App\Http\Controllers\api;

use App\Meeting;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
            return Meeting::all();


        
    }

    public function create(Request $request)
    {
   
        // return view('welcome', ['votes' => Vote::all(), 'meetings' => Meeting::all()]);
        // return response()->json([
        //     'meetings' => Meeting::latest()->get()
        // ]);
        $meeting = new Meeting();
        $meeting->name = $request->name;
        $meeting->description = $request->description;
        $meeting->save();
        return response($meeting->jsonSerialize(), Response::HTTP_CREATED);
    }

}