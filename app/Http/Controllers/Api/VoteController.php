<?php

namespace App\Http\Controllers\api;

use App\Vote;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('welcome');
         return response(Vote::all()->jsonSerialize(), Response::HTTP_OK);
        
    }

    public function create(Request $request)
    {
   
        // return view('welcome', ['votes' => Vote::all(), 'meetings' => Meeting::all()]);
        // return response()->json([
        //     'meetings' => Meeting::latest()->get()
        // ]);
        $vote = new Vote();
        $vote->title = $request->title;
        $vote->question = $request->question;
        $vote->choice1 = $request->choice1;
        $vote->choice2 = $request->choice2;
        $vote->save();
        return response($vote->jsonSerialize(), Response::HTTP_CREATED);
    }

}