<?php

namespace App\Http\Controllers;

use App\Vote;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('test');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),
        [
            'title' => 'required',
            'question' => 'required',
            'choice1' => 'required',
            'choice2' => 'required'
        ]);

        Vote::Create(
        [
            'title' => request('title'),
            'question' => request('question'),
            'choice1' => request('choice1'),
            'choice2' => request('choice2')
        ]);

        return ['message' => 'Vote Created !'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vote  $meeting
     * @return \Illuminate\Http\Response
     */
}