<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = array(
        'title', 'question', 'choice1', 'choice2'
    );
}
